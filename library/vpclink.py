#!/usr/bin/python

ANSIBLE_METADATA = {'metadata_version': '2.0',
                    'status': ['preview'],
                    'supported_by': 'Wouter'}

DOCUMENTATION = '''
---
module: vpc_link
version_added: '2.4.2'
short_description: Searches for AMIs to obtain the AMI ID and other information
description:
  - Creates API Gateway VPC Link objects and attaches methods to it
requirements:
  - "python >= 2.7"
  - boto3


Ok, so this is a nutcase module:
 - fetch all vpclinks and return them name/nlbarns dict wise and vise versa
 - creates vpclinks when nlbarn is not found
 - 

'''

import boto3
import ansible.module_utils.ec2
from ansible.module_utils.ec2 import AWSRetry, boto_exception
import json
from pprint import pformat
# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *
import time

try:
    import botocore
    HAS_BOTOCORE = True
except ImportError:
    HAS_BOTOCORE = False
try:
    from botocore.exceptions import ClientError
except ImportError:
    pass  # caught by imported HAS_BOTO3

def get_vpc_link(client, module, linkid):
    try:
        link = client.get_vpc_link(vpcLinkId=linkid)
    except ClientError as e:
        module.fail_json(msg=e.message, **camel_dict_to_snake_dict(e.response))
    except Exception as e:
        module.fail_json(msg=e.message + '\n\n -- check your boto3 version, you might need 1.5.14 or higher!')
    return link

def del_vpc_link(client, module, linkid):
    try:
        link = client.delete_vpc_link(vpcLinkId=linkid)
    except ClientError as e:
        module.fail_json(msg=e.message, **camel_dict_to_snake_dict(e.response))
    except Exception as e:
        module.fail_json(msg=e.message + '\n\n -- check your boto3 version, you might need 1.5.14 or higher!')
    while link['status'] == 'DELETING':
        time.sleep(3)
        link = get_vpc_link(client, module, link['id'])
    return link

def list_vpclinks(client, module):
    try:
        vpclinks_paginator = client.get_paginator('get_vpc_links')
        res = vpclinks_paginator.paginate().build_full_result()
    except ClientError as e:
        module.fail_json(msg=e.message, **camel_dict_to_snake_dict(e.response))
    except Exception as e:
        module.fail_json(msg=e.message + '\n\n -- check your boto3 version, you might need 1.5.14 or higher!')
    # Make proper dict from this
    vpclinks = {}
    for link in res['items']:
        li = {
            "nlb_arn": link['targetArns'][0],
            "name": link['name'],
            "id": link['id'],
        }
        if 'description' in link:
            li['description'] = link['description']
        if 'status' in link:
            li["status"] = link["status"]
        if 'statusMessage' in link:
            li["statusMessage"] = link["statusMessage"]
        vpclinks[link['name']] = li
        vpclinks[link['targetArns'][0]] = li
    return vpclinks

def create_vpc_link(client, module, name, nlb_arn):
    try:
        res = client.create_vpc_link(
                name=name,
                description='Ansible generated VPC Link',
                targetArns=[
                    nlb_arn
                ]
        )
    except ClientError as e:
        module.fail_json(msg=e.message, **camel_dict_to_snake_dict(e.response))
    except Exception as e:
        module.fail_json(msg=e.message + '\n\n -- Unknown error here. You are hosed.')
    return res

def get_resources(client, module, api):
    try:
        # Figure out api id
        api_paginator = client.get_paginator('get_rest_apis')
        apis = api_paginator.paginate().build_full_result()
        res_apis = {}
        for a in apis['items']:
            res_apis[ a['name'] ] = a['id']
        # Figure out which one we want.
        if not api in res_apis:
            module.fail_json(msg="Could not find requested API {0} in list of rest apis!".format(api))
        api_id = res_apis[api]
        # Get resources
        resources_paginator = client.get_paginator('get_resources')
        res = resources_paginator.paginate(restApiId=api_id).build_full_result()
        results = {
          '__api_id__' : api_id
        }
        for r in res['items']:
            results[ r['path'] ] = r
    except ClientError as e:
        module.fail_json(msg=e.message, **camel_dict_to_snake_dict(e.response))
    except Exception as e:
        module.fail_json(msg=e.message + '\n\n -- Unknown error here. You are hosed.')
    return results

def main():
    argument_spec = ansible.module_utils.ec2.ec2_argument_spec()
    argument_spec.update(dict(
            vpclinks = dict(required=False, default={}, type='dict'),
            api      = dict(required=False, default={}, type='dict'),
            state    = dict(required=False, default='present', type='str'),
        )
    )

    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True,
    )

    vpclinks = module.params.get('vpclinks')
    api      = module.params.get('api')
    state    = module.params.get('state')

    changed = False

    if not HAS_BOTO3:
        module.fail_json(msg='Python module "boto3" is missing, please install boto3')
    if not HAS_BOTOCORE:
        module.fail_json(msg='Python module "botocore" is missing, please install it')

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
    try:
        client = boto3_conn(module, conn_type='client', resource='apigateway', region=region, endpoint=ec2_url, **aws_connect_kwargs)
    except botocore.exceptions.NoRegionError:
        module.fail_json(msg="Region must be specified as a parameter, in AWS_DEFAULT_REGION environment variable or in boto configuration file")
    except (botocore.exceptions.ValidationError, botocore.exceptions.ClientError) as e:
        fail_json_aws(module, e, msg="connecting to AWS")

    # Fetch existing vpc links
    vlids = list_vpclinks(client, module)
    results = {
        'warnings':[],
        'vpclinks': vlids,
        'changed': False,
        'output': 'STILL BEING IMPLEMENTED'
    }

    if len(vpclinks.keys()) > 0:
        for link in vpclinks.keys():
            # See if it exists
            if link in results['vpclinks']:
                if results['vpclinks'][link]['status'] ==  "FAILED":
                    # Delete and recreate
                    del_vpc_link(client, module, results['vpclinks'][link]['id'])

                # Exist, skip creation.
                # module.fail_json(msg='VPC Link {} exists - not recreating'.format(link))
                continue
            # Does not exist, create.
            if vpclinks[link] in results['vpclinks']:
                # Exists under other name, use that instead!
                results['warnings'].append("WARNING: VPC Link exists but under other name -- found '{0}' but requested was '{1}'!".format(results["vpclinks"][vpclinks[link]]["name"], link))
                results['vpclinks'][link] = results["vpclinks"][vpclinks[link]]
                vlids[link] = results["vpclinks"][vpclinks[link]]
                continue
            res = create_vpc_link(client, module, link, vpclinks[link])
            # While it's creating we wait.
            while res['status'] == 'PENDING':
                time.sleep(3)
                res = get_vpc_link(client, module, res['id'])
            if res['status'] == 'FAILED':
                module.fail_json(msg="Tried to create vpclink, failed :-(\n{}".format(pformat(res)))
                # del_vpc_link(client, module, results['vpclinks'][link]['id'])
            results['vpclinks'][link] = {
                "nlb_arn": res['targetArns'][0],
                "name": res['name'],
                "id": res['id'],
            }
            changed = True
            results['warnings'].append("Tried to create vpclink, result:\n{}".format(pformat(res)))

    # Now that every VPC Link has been created, let's make sure the methods are also attached to it.
    if len(api.keys()) > 0:

        # self.apis[api]["vpclink"][ method["vpclink"] ]["methods"].append(method["name"])
        #  ->    apis["Test2Pay"]["vpclink"]["TestVPCLink"]["methods"] = [ { "CreateWallet", ...]

        # For that we'll need the resource ids, so fetch them
        rids = get_resources(client, module, api["key"])
        # results['warnings'].append("Resource dump:\n{0}".format(pformat(rids)))

        # module.fail_json(msg="DEBUG:\n{0}".format(pformat(api["value"]["vpclink"]))
        for link in api["value"]["vpclink"].keys():
            # results['warnings'].append("vpclink debug:\n{0}".format(pformat(api["value"]["vpclink"][link])))
            if link not in vlids:
                results['warnings'].append("Could not find link {1} in vlids, dump:\n{0}".format(pformat(vlids), link))
                module.exit_json(**results)

            for method in api["value"]["vpclink"][link]['methods']:
                # rids[ method["resourcepath"] ]
                # Update the method:
                # [{"op":"replace","path":"/connectionType","value":"VPC_LINK"},{"op":"replace","path":"/connectionId","value":"b81ryg"}]
                old = client.get_integration(
                        restApiId= rids['__api_id__'],
                        resourceId=rids[ method['resourcepath'] ]['id'],
                        httpMethod=method['type']
                )
                if old['connectionType'] != 'VPC_LINK' or old['connectionId'] != vlids[link]['id']:
                    res = client.update_integration(
                            restApiId= rids['__api_id__'],
                            resourceId=rids[ method['resourcepath'] ]['id'],
                            httpMethod=method['type'],
                            patchOperations=[
                                {
                                    'op': 'replace',
                                    'path': "/connectionType",
                                    'value': 'VPC_LINK',
                                },
                                {
                                    'op': 'replace',
                                    'path': "/connectionId",
                                    'value': vlids[link]['id'], # Vpclink id
                                },
                            ]
                    )
                    changed = True
                    results['warnings'].append("Link[{0}] - method {1} - now connection:{3} on connectionId:{2}".format(link, method['resourcepath'], res['connectionId'], res['connectionType']))
                else:
                    # Already set.
                    pass

    # except TypeError:
    #     module.fail_json(msg="Please supply numeric values for sort_start and/or sort_end")

    results['changed'] = changed
    module.exit_json(**results)

if __name__ == '__main__':
    main()
