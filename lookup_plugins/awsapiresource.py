# Ansible lookup module to calculate VPC CIDR ranges
#
# Input: network + distribution weights + azs
# Output: dictionary of subnet type with a list of {az:name, cidr: CIDR} dicts
# Strategy is quite simple right now and could use improvement. Right now the number of IPs are divided, rounded into subnets and rescaled to make it fit if necessary.
# There will be slack due to CIDR being annoying, can't be prevented.

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleError, AnsibleParserError
from ansible.plugins.lookup import LookupBase
from pprint import pformat

try:
  from __main__ import display
except ImportError:
  from ansible.utils.display import Display
  display = Display()

'''
Input should look a bit like this:
  restapi:
    Test2Pay:
      defaults:
        request:
          headers:
            apikey: "'somekeyhere'"
        response:
          contenttype: "application/json"
          headers:
            "Server": "Nginx/1.9"
      stages:
        - stage: "test"
          variables:
            stagetype: "test"
      methods:
        - path: "/Account/CreateAccount"
          type: POST
          name: "CreateAccount"
          endpoint: "https://${stagevariables.stage}-account.ease2.com/create"
        - path: "/Account/GetUser"
          type: GET
          name: "GetUser"
          endpoint: "https://${stagevariables.stage}-account.ease2.com/user"
          url_params:
            - "session_id"
            - "betonMolen"
          request:
            headers:
              apikey: "'someoverride'"
          response:
            contenttype: "text/html"
            headers:
              "Server": "Apache 2.0"
  vpclinks:
    TestVPCLink: "arn::bla:nlb"
    MyOtherVPCLink: "arn::bla:nlb22"

All methods will be parsed and require resources will be returned.
Output will be input with added resources section:
  resources:
    - name: RS_Some
      path: some
      parent:
    - name: RS_SomeVery
      path: Very
      parent: RS_Some

  methods:
    - path: "/Some/Very/Long/Path/.."
      (..)
      resource: RS_SomeVeryLongPath...

The toplevel resources will be put in the main template, descendants with their methods go to the child template(s).
In order to split up the resources between child templates we'll add some bookkeeping things to the variables:
 resources - nestlevel (0==main, 1-... is sub template)
 cloudformation - stackcount (0 == only main, 1 == child as well)

'''

class LookupModule(LookupBase):
    def fixNestLevel(self, level=0):
        '''
           This function splits up the resources(+methods) into several levels.
           The top level (level 0) has all the resources that are directly connected to the restapi.
           The next levels are split per top level.
           I.e.
           /Account/bladie/bla
           /Merchant/bla/diebla/
           /Payment/more/stuff
           ->
           Account, Merchant, Payment in toplevel (level 0)
           Account/bladie and Account/bladie/bla in level 1
           Merchant/bla and Merchent/bla/diebla in level 2
           Payment/more and Payment/more/stuff in level 3
        '''

        nestCount = {
            # level: count
        }
        levelMap = {
            # rsname: level
        }
        lastLevel = 0
        api = self.api
        if level == 0:
            # Fine, we need to split them up. Walk through resources, skip top level and put the rest a level lower.
            for rsname in sorted(self.apis[api]["resources"].keys()):
                if self.apis[api]["resources"][rsname]["parent"] == "RESTAPI":
                    # Find parents, they need to remain in the main template. Add them to levelMap.
                    self.apis[api]["resources"][rsname]["nestlevel"] = 0
                    levelMap[rsname] = lastLevel + 1
                    lastLevel += 1
            for rsname in sorted(self.apis[api]["resources"].keys()):
                if self.apis[api]["resources"][rsname]["parent"] != "RESTAPI":
                    # Child, fix nestlevel
                    # First find top parent
                    parent = self.apis[api]["resources"][rsname]["parent"]
                    while parent != "RESTAPI":
                        lastparent = parent
                        parent = self.apis[api]["resources"][parent]["parent"]
                    self.apis[api]["resources"][rsname]["nestlevel"] = levelMap[lastparent]
        else:
            raise AnsibleError("After nesting we need a second iteration, but this is not yet implemented! (buy me time ;))")
        self.apis[api]["cloudformation"]["lastlevel"] = [ lastLevel ]
        self.apis[api]["cloudformation"]["nestlevels"] = list(range(1, lastLevel + 1))

        # Get the count
        for rsname in sorted(self.apis[api]["resources"].keys()):
            nestlevel = str(self.apis[api]["resources"][rsname]["nestlevel"])
            if nestlevel not in nestCount:
                nestCount[nestlevel ] = 0
            nestCount[nestlevel] += 1
            nestCount[nestlevel] += len(self.apis[api]["resources"][rsname]["methods"])
        # Check the count
        for nestlevel in nestCount:
            if nestCount[nestlevel] > 190:
                # We are still screwed. Reiterate.
                self.fixNestLevel(self, level + 1)
                return

    def run(self, terms, variables=None, **kwargs):
        '''
        # lookup sample:
        - name: lookup sts caller identity in the current region
          debug: msg="{{ lookup('awsapiresource', vartree) }}"
        # Used as:
        - name: resolve api resources
          set_fact:
            apis: "{{ lookup('awsapiresource', myapivar) }}"
        '''

        ret = {}
        response = {}
        # Input debug:
        # raise AnsibleError("Terms: %s\nVariables: %s\n" % (pformat(terms), pformat(variables)))

        self.apis = terms[0]
        # Create hash of resources per api, i.e. resources{apiname}{some}{very}{long}{path}{with}{many}{resources}
        for api in sorted(self.apis.keys()):
            nestLevel = 0 # Top template
            self.apis[api]["resources"] = {}
            if not "vpclink" in self.apis[api]:
                self.apis[api]["vpclink"] = {}

            self.apis[api]["cloudformation"] = {
                "totalresources": 0
            }
            defaults = {}
            if "defaults" in self.apis[api]:
                defaults = self.apis[api]["defaults"]
            for method in sorted(self.apis[api]["methods"], key = lambda x: x['path']):
                pathparts = method["path"].lstrip("/").split("/") # '/Some/Very/Long/Path/With/Many/Resources' -> ['Some', 'Very', ...]
                rsname = "ApiRes" + ''.join(pathparts)
                method["rsname"] = rsname
                method["name"] = "ApiMtd" + ''.join(pathparts)
                self.apis[api]["cloudformation"]["totalresources"] += 1

                # Add defaults to method when applicable
                if not "request" in method:
                    method["request"] = {}
                if not "response" in method:
                    method["response"] = {}

                if "request" in defaults:
                    if "headers" in defaults["request"]:
                        # Only add default headers if method has not defined any.
                        if "headers" not in method["request"]:
                            method["request"]["headers"] = defaults["request"]["headers"]
                        # TODO: more request defaults here
                if "response" in defaults:
                    if "headers" in defaults["response"]:
                        if "headers" not in method["response"]:
                            method["response"]["headers"] = defaults["response"]["headers"]
                    if "contenttype" in defaults["response"]:
                        if "contenttype" not in method["response"]:
                            method["response"]["contenttype"] = defaults["response"]["contenttype"]
                        # TODO: more response defaults here
                if "vpclink" in defaults:
                    if "vpclink" not in method:
                        method["vpclink"] = defaults["vpclink"]

                if "vpclink" in method:
                    if method["vpclink"] not in self.apis[api]["vpclink"]:
                        self.apis[api]["vpclink"][method["vpclink"]] = {}
                        self.apis[api]["vpclink"][method["vpclink"]]["methods"] = []
                    self.apis[api]["vpclink"][method["vpclink"]]["methods"].append({ "resourcepath": method["path"], "type": method["type"] })

                if not rsname in sorted(self.apis[api]["resources"].keys()):
                    # Resource not yet created, make sure parents also exist.
                    prsname = "ApiRes"
                    oldprsname = "RESTAPI"
                    for pathpiece in pathparts:
                        # Create parents
                        prsname = prsname + pathpiece
                        if prsname not in sorted(self.apis[api]["resources"].keys()):
                            self.apis[api]["resources"][prsname] = {
                                "parent": oldprsname,
                                "pathpart": pathpiece,
                                "nestlevel": nestLevel,
                                "methods": []
                            }
                            self.apis[api]["cloudformation"]["totalresources"] += 1
                        oldprsname = prsname
                self.apis[api]["resources"][rsname]["methods"].append(method["name"])

            # If required, fix nestlevels.
            if self.apis[api]["cloudformation"]["totalresources"] > 190:
                self.api = api
                self.fixNestLevel()

        display.vvv("Result for API GW:" + pformat(self.apis))
        return [self.apis]
