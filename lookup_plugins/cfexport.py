# (c) 2018, Wouter de Geus <wdegeus(at)mirabeau.nl>
# Copy/pasted/molested a bunch from aws_ssm.py
#
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.module_utils._text import to_native
from ansible.module_utils.ec2 import HAS_BOTO3, boto3_tag_list_to_ansible_dict
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase

try:
    from botocore.exceptions import ClientError
    import botocore
    import boto3
except ImportError:
    pass  # will be captured by imported HAS_BOTO3

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

def _boto3_conn(region, credentials):
    if 'boto_profile' in credentials:
        boto_profile = credentials.pop('boto_profile')
    else:
        boto_profile = None

    try:
        connection = boto3.session.Session(profile_name=boto_profile).client('cloudformation', region, **credentials)
    except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
        if boto_profile:
            try:
                connection = boto3.session.Session(profile_name=boto_profile).client('cloudformation', region)
            except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
                raise AnsibleError("Insufficient credentials found.")
        else:
            raise AnsibleError("Insufficient credentials found.")
    return connection

class LookupModule(LookupBase):
    def run(self, terms, variables=None, boto_profile=None, aws_profile=None,
            aws_secret_key=None, aws_access_key=None, aws_security_token=None, region=None):
        '''
            :arg terms: a list of lookups to run.
                e.g. ['parameter_name', 'parameter_name_too' ]
            :kwarg variables: ansible variables active at the time of the lookup
            :kwarg aws_secret_key: identity of the AWS key to use
            :kwarg aws_access_key: AWS seret key (matching identity)
            :kwarg aws_security_token: AWS session key if using STS
            :kwarg region: AWS region in which to do the lookup
            :returns: A list of parameter values or a list of dictionaries if bypath=True.
        '''

        if not HAS_BOTO3:
            raise AnsibleError('botocore and boto3 are required for cfexport lookup.')

        ret = []
        response = {}
        cf_dict = {}

        credentials = {}
        if aws_profile:
            credentials['boto_profile'] = aws_profile
        else:
            credentials['boto_profile'] = boto_profile
        credentials['aws_secret_access_key'] = aws_secret_key
        credentials['aws_access_key_id'] = aws_access_key
        credentials['aws_session_token'] = aws_security_token

        client = _boto3_conn(region, credentials)

        # Grab all cloudformation stacks
        paginator = client.get_paginator('describe_stacks')
        from pprint import pformat
        exports = {}
        for outputlist in (x['Outputs'] for x in paginator.paginate().build_full_result().get('Stacks', []) if 'Outputs' in x):
            for output in outputlist:
                display.vvvv("Output debug: %s" % (pformat(output)))
                if 'ExportName' in output:
                    exports[output['ExportName']] = output['OutputValue']
        display.vvv("CF_Export path lookup found %d exports: %s" % (len(exports), pformat(exports)))

        if len(terms) == 0:
            display.vvv("CFExport lookup: no specific export specified, so returning all %d exports." % (len(exports)))
            return [exports]

        for term in terms:
            if term in exports:
                ret.append({ term: exports[term] })

        display.vvv("CFExport lookup: returning matched exports only since terms were given (%d matched): %s " % (len(ret), str(ret)))
        if len(terms) == 1:
            if len(ret) == 1:
                return ret[0].values()
            return None
        return ret

