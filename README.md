Role Name
=========

Create Cloudformation stack for API Gateway and methods

Requirements
------------

Concept
-------

The aws-setup role creates an APIGateway::Account to handle log permissions, so we're not doing that in this role.
In the variables you must define the entire API Gateway with all stages and methods, see below.

Due to the limited brain capacity of the folks that crafted AWS Cloudformation they decided to mess up both Api Gateway
deployments and also include the handy resource limit of 200 resources per cloudformation.
To works around this (since 200 is not that high as it may seem when you need to implement both a resource and a method per URL)
we create a nested stack for the methods.
So the main template will create:
 - restapi
 - resources
 - deployments (depending on the method stack)
 - nested(s) stack with all methods

Finally, due to vpclink not being available to cloudformation yet we create and update the methods manually through aws cli commands.

Role Variables
--------------

_Role specific_
```
aws_api_gateway_params:
  restapi:
    MyFirstAPI:
      stages:
        - stage: "prod"
          variables:
            foo: "barprod"
        - stage: "test"
          variables:
            bar: "qux"
      defaults:
        vpclink: "MyVPCLink"
      methods:
        - path: "/Account/CreateAccount"
          type: POST
          name: "CreateAccount"
          endpoint: "https://${stageVariables.stagetype}-account.mydomain.com/account-create"
          rheaders:
            mysecretheader: "'magicvalue-note-the-quotes-around-it'"
        - path: "/Google/Search"
          type: GET
          endpoint: "https://google.com/"
        - [...]
        - path: "/Some/Very/Long/Path/With/Many/Resources"
          type: GET
          endpoint: "https://${stageVariables.stagetype}-account.mydomain.com/something/silly"
          vpclink: "MyOtherVPCLink"
  vpclinks:
    MyVPCLink: "{{ nlb_arn }}"
    MyOtherVPCLink: "{{ nlb2_arn }}"

VPCLinks are created if they don't exist based on NLB Arn.

```

_General_
```
  aws_region      : <aws region, eg: eu-west-1>
  owner           : <owner, eg: mirabeau>
  account_name    : <customer name>
  environment_type: <environment>
  environment_abbr: <environment abbriviation>
```

Dependencies
------------
 * aws-setup
 * aws-vpc

Example Playbook
----------------
    - hosts: localhost
      roles:
         - { role: aws-api-gateway }

License
-------
BSD

Author Information
------------------
Wouter de Geus <wdegeus@mirabeau.nl>
